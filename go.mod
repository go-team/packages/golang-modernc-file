module modernc.org/file

go 1.18

require (
	modernc.org/internal v1.0.7
	modernc.org/mathutil v1.5.0
)

require (
	github.com/edsrzf/mmap-go v1.1.0 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	modernc.org/fileutil v1.1.2 // indirect
)
